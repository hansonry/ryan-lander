extends Object
class_name WfcTile

var top_list : Array[WfcTile] = []
var bottom_list : Array[WfcTile] = []
var left_list : Array[WfcTile] = []
var right_list : Array[WfcTile] = []
var ref = null
var weight : float = 1

func _init(_ref = null, _weight : float = 1):
	ref = _ref
	weight = _weight


func _append_to_set(dest: Array[WfcTile], src: Array[WfcTile]):
	for t in src:
		if not dest.has(t):
			dest.append(t)

func add_to_top_set(top_set: Array[WfcTile]):
	_append_to_set(top_set, top_list)

func add_to_bottom_set(bottom_set: Array[WfcTile]):
	_append_to_set(bottom_set, bottom_list)

func add_to_left_set(left_set: Array[WfcTile]):
	_append_to_set(left_set, left_list)

func add_to_right_set(right_set: Array[WfcTile]):
	_append_to_set(right_set, right_list)

