extends Node2D

@onready var _pole : RigidBody2D = $Pole
@onready var _top : RigidBody2D = $Top
@export var pole_joint : PinJoint2D = null
@export var top_joint : PinJoint2D = null

var connected : bool = true
var hit : bool = false

# Called when the node enters the scene tree for the first time.
func _ready():
	assert(pole_joint != null, "pole_joint must be set")
	assert(top_joint != null, "top_joint must be set")
	
	pass # Replace with function body.

func _on_hit():
	hit = true
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _physics_process(delta):
	if !connected:
		return
	_pole.apply_torque(-_pole.global_rotation * 1500)
	_top.apply_torque((-_top.global_rotation + _pole.global_rotation) * 1000)
	
	if abs(_pole.global_rotation) > PI / 3 or abs(_top.global_rotation - _pole.global_rotation) > PI / 3:
		connected = false
		top_joint.queue_free()
		pole_joint.queue_free()
		_top.apply_torque_impulse((_top.global_rotation - _pole.global_rotation) * 100)
		_pole.apply_torque_impulse(_pole.global_rotation * 100)

func _on_top_body_entered(body):
	return
	if body is Lander:
		_top.gravity_scale = 1
		if not hit:
			_on_hit()


func _on_pole_body_entered(body):
	return
	if body is Lander:
		_pole.gravity_scale = 1
		_top.gravity_scale = 1
		if not hit:
			_on_hit()
