@tool
extends StaticBody2D
class_name GroundSection

@export var thickness: float = 10 :
	set(value):
		thickness = value
		_updateDeltas()

@export var delta : Vector2 = Vector2(20, 0) :
	set(value):
		delta = value
		_updateDeltas()


@onready var _line : Line2D = $Line2D
@onready var _col_shape: CollisionShape2D = $CollisionShape2D


# Called when the node enters the scene tree for the first time.
func _ready():
	_updateDeltas()
	pass # Replace with function body.

func _updateDeltas():
	if is_node_ready():
		var round_delta = delta.round()
		var length = delta.length()
		var angle = atan2(delta.y, delta.x)
		
		_line.points[1] = round_delta
		
		var ca = cos(angle)
		var sa = sin(angle)
		
		_col_shape.position.y = ca * (thickness / 2) + sa * (length / 2)
		_col_shape.position.x = ca * (length / 2) - sa * (thickness / 2)
		_col_shape.rotation = angle
		
		_col_shape.shape.size.x = length
		_col_shape.shape.size.y = thickness
		


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
