extends CanvasLayer
class_name HUD

@onready var _vel_map: Sprite2D = $VelocityMap
@onready var _vel_hairs : Sprite2D = $VelocityHairs
@onready var _vel_val : Label = $LblVelValue
@onready var _fuel_bar : ProgressBar = $FuelBar
@onready var _grid_coords: Label = $LblGridCoords

var _size :Vector2 = Vector2.ZERO
var _center :Vector2 = Vector2.ZERO

var _ring_1_max_speed = 200

# Called when the node enters the scene tree for the first time.
func _ready():
	_size.x = _vel_map.texture.get_width() * _vel_map.scale.x
	_size.y = _vel_map.texture.get_width() * _vel_map.scale.y
	_center = _size / 2 + _vel_map.position
	pass # Replace with function body.

func set_player_fuel(fuel : float):
	_fuel_bar.value = fuel

func set_player_velocity(vel : Vector2):
	var speed = vel.length()
	_vel_val.text = "%d" % round(speed)
	var draw_at : Vector2 = Vector2.ZERO
	if speed > 0.0001:
		if speed < _ring_1_max_speed:
			draw_at = vel * 64 / _ring_1_max_speed
		else:
			var speed_offset = speed - _ring_1_max_speed
			var norm = vel / speed
			var speed_offset_scaled = sqrt(speed_offset) / 50
			
			var total_speed = 64 + speed_offset_scaled  * 64 
			total_speed = min(total_speed, 128)
			draw_at = norm * total_speed
			
	_vel_hairs.position.x = _center.x + draw_at.x
	_vel_hairs.position.y = _center.y + draw_at.y + 1
	
	pass

func set_player_grid_coords(pos :Vector2i):
	_grid_coords.text = "(%d, %d)" % [pos.x, pos.y]

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
