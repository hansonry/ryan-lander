@tool
extends Node2D
class_name MapSectionRandom



var _section_x_size = 0

const _ground_section_resource = preload("res://ground_section.tscn")
const _landing_pad_resource = preload("res://landing_pad.tscn")



@export var regenerate_button: bool = false:
	set(value):
		_editor_generate()

@export var outpost_scene: PackedScene = null:
	set(value):
		outpost_scene = value
		_editor_generate()

@export var width : float = 1024:
	set(value):
		width = value
		_editor_generate()
		
@export var height : float = 256:
	set(value):
		height = value
		_editor_generate()

@export var max_landing_size: int = 400:
	set(value):
		max_landing_size = value
		_editor_generate()


@export var  large_segment_count: int = 5:
	set(value):
		large_segment_count = value
		_editor_generate()
		
@export var large_segment_subdivision: int = 2:
	set(value):
		large_segment_subdivision = value
		_editor_generate()


@export var auto_fill_in: bool = true

var _heights: Array = []
var _between_larget_segment_count = 0
var _point_count = 0
var rng :RandomNumberGenerator = RandomNumberGenerator.new()
var _generation_started = false

var first_height: float = 0:
	set(value):
		if _heights.size() > 0:
			_heights[0] = value
	get:
		if _heights.size() > 0:
			return _heights[0]
		return 0

var last_height: float = 0:
	set(value):
		if _heights.size() > 0:
			_heights[-1] = value
	get:
		if _heights.size() > 0:
			return _heights[-1]
		return 0

func _editor_generate():
	if Engine.is_editor_hint():
		generate()

func generate():
	# Clean up previous generation if any
	var children = get_children()
	for c in children:
		remove_child(c)
		c.queue_free()
		
	# Compute new Values
	_between_larget_segment_count = pow(2, large_segment_subdivision)
	_point_count = large_segment_count * _between_larget_segment_count + 1
	_section_x_size = width / (_point_count - 1)

	# Setup array
	_heights.resize(_point_count)
	_heights.fill(null)
	
	_generation_started = true
	# Generate
	_generate_large_features()
	if auto_fill_in or Engine.is_editor_hint():
		fill_in_rest()

# Called when the node enters the scene tree for the first time.
func _ready():
	if not _generation_started:
		generate()


func _make_landing_spot():
	var outpost : Outpost = outpost_scene.instantiate() as Outpost
	var max_size = min(ceil(max_landing_size / _section_x_size), _point_count - 2)
	var min_size = max(ceil(outpost.width / _section_x_size), 2)
	var size = rng.randi_range(min_size, max_size)
	#print(size * _section_x_size)
	var start = rng.randi_range(1, _point_count - size - 2)
	var low = _heights[start - 1]
	var high = _heights[start + size]
	var height = (low + high) / 2
	for i in range(start, start + size + 1):
		_heights[i] = height
	
	var spot_size = size * _section_x_size
	
	var placement_varience = spot_size - outpost.width
	var x = start * _section_x_size + rng.randi_range(0, placement_varience)
	outpost.position.x = x
	outpost.position.y = height
	add_child(outpost)
	pass


func fill_in_rest():
	_generate_small_features()
	if outpost_scene != null:
		_make_landing_spot()
	_instance_parts()

func _recusive_generate(from, to, height_delta):
	var jump = to - from
	var base = _heights[from]
	var range = _heights[to] - base
	var index = from + jump / 2
	var new_height_delta = height_delta / 2
	_heights[index] = base + (range / 2) + rng.randf_range(-new_height_delta, new_height_delta)
	if jump > 2:
		_recusive_generate(from,  index, new_height_delta)
		_recusive_generate(index, to,    new_height_delta)

	
func _generate_small_features():
	for hIndex in range(0, _point_count - _between_larget_segment_count, _between_larget_segment_count):
		_recusive_generate(hIndex, hIndex + _between_larget_segment_count, height / 2)


func _instance_parts():
	var x = 0
	for hIndex in (_point_count - 1):
		var sx = x
		var sy = _heights[hIndex]
		var dx = _section_x_size
		var dy = _heights[hIndex + 1] - sy
		var section : GroundSection = _ground_section_resource.instantiate()
		section.position = Vector2(sx, sy)
		section.delta = Vector2(dx, dy)
		add_child(section)
		x = x + _section_x_size
		
func _generate_large_features():
	for hIndex in range(0, _point_count, _between_larget_segment_count):
		_heights[hIndex] = rng.randf_range(0, height)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
