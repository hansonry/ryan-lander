extends Node2D
class_name RandomMap


var rng: RandomNumberGenerator = RandomNumberGenerator.new()

@export var width : int = 20
@export var height : int = 20

const _ps_outpost_big = preload("res://outposts/outpost_big.tscn")
const _ps_outpost_medium = preload("res://outposts/outpost_medium.tscn")
const _ps_outpost_makeshift = preload("res://outposts/outpost_makshift.tscn")


const _ms_bottom = preload("res://map_sections/map_section_bottom.tscn")
const _ms_top = preload("res://map_sections/map_section_top.tscn")
const _ms_left = preload("res://map_sections/map_section_left.tscn")
const _ms_right = preload("res://map_sections/map_section_right.tscn")
const _ms_inside = preload("res://map_sections/map_section_inside_corner_ur.tscn")
const _ms_outside = preload("res://map_sections/map_section_outside_corner_ur.tscn")
const _ms_random = preload("res://map_sections/map_section_random.tscn")

func _get_position_offset(pos: Vector2i) -> Vector2i:
	return Vector2i(pos.x * 1024, pos.y * 256)

func _get_index_of(pos:Vector2i) -> int:
	if pos.x < 0 or pos.x >= width or pos.y < 0 or pos.y >= height:
		return -1
	return pos.x + pos.y * width

func _generate():
	var hx = 1024
	var hy = 256
	var random_tiles = []
	random_tiles.resize(width * height)
	random_tiles.fill(null)
	var t_outside_empty = WfcTile.new(func(_pos):
		return null, 0.1
	)
	var t_inside_empty = WfcTile.new(func(_pos):
		return null, 0.2
	)
	var t_bottom = WfcTile.new(func(pos):
		var tile = _ms_random.instantiate()
		tile.scale.y = -1
		tile.position = _get_position_offset(pos) + Vector2i(0, hy)
		tile.rng = rng
		tile.auto_fill_in = false
		tile.generate()
		return tile
	)
	var t_top = WfcTile.new(func(pos):
		var tile = _ms_random.instantiate()
		tile.position = _get_position_offset(pos)
		tile.rng = rng
		tile.auto_fill_in = false
		tile.generate()
		return tile
	)

	var t_left = WfcTile.new(func(pos):
		var tile = _ms_left.instantiate()
		tile.position = _get_position_offset(pos)
		return tile, 0.5
	)
	var t_right = WfcTile.new(func(pos):
		var tile = _ms_right.instantiate()
		tile.position = _get_position_offset(pos)
		return tile, 0.5
	)
	var t_inside_ur = WfcTile.new(func(pos):
		var tile = _ms_inside.instantiate()
		tile.position = _get_position_offset(pos)
		return tile
	)
	var t_inside_ul = WfcTile.new(func(pos):
		var tile = _ms_inside.instantiate()
		tile.scale = Vector2(-1, 1)
		tile.position = _get_position_offset(pos) + Vector2i(hx, 0)
		return tile
	)
	var t_inside_ll = WfcTile.new(func(pos):
		var tile = _ms_inside.instantiate()
		tile.scale = Vector2(-1, -1)
		tile.position = _get_position_offset(pos) + Vector2i(hx, hy)
		return tile
	)

	var t_inside_lr = WfcTile.new(func(pos):
		var tile = _ms_inside.instantiate()
		tile.scale = Vector2(1, -1)
		tile.position = _get_position_offset(pos) + Vector2i(0, hy)
		return tile
	)
	
	var t_outside_ur = WfcTile.new(func(pos):
		var tile = _ms_outside.instantiate()
		tile.position = _get_position_offset(pos)
		return tile
	)
	var t_outside_ul = WfcTile.new(func(pos):
		var tile = _ms_outside.instantiate()
		tile.scale = Vector2(-1, 1)
		tile.position = _get_position_offset(pos) + Vector2i(hx, 0)
		return tile
	)
	var t_outside_ll = WfcTile.new(func(pos):
		var tile = _ms_outside.instantiate()
		tile.scale = Vector2(-1, -1)
		tile.position = _get_position_offset(pos) + Vector2i(hx, hy)
		return tile
	)

	var t_outside_lr = WfcTile.new(func(pos):
		var tile = _ms_outside.instantiate()
		tile.scale = Vector2(1, -1)
		tile.position = _get_position_offset(pos) + Vector2i(0, hy)
		return tile
	)

	var list : Array[WfcTile] = [
		t_outside_empty,
		t_inside_empty,
		t_top,
		t_bottom,
		t_left,
		t_right,
		t_inside_ur,
		t_inside_ul,
		t_inside_lr,
		t_inside_ll,
		t_outside_ur,
		t_outside_ul,
		t_outside_lr,
		t_outside_ll,
	]
	
	var top_facing: Array[WfcTile] = [
		t_top, t_inside_ul, t_inside_ur
	]
	var bottom_facing: Array[WfcTile] = [
		t_bottom, t_inside_ll, t_inside_lr
	]
	var left_facing: Array[WfcTile] = [
		t_left, t_inside_ul, t_inside_ll
	]
	var right_facing: Array[WfcTile] = [
		t_left, t_inside_ur, t_inside_lr
	]

	for lf in left_facing:
		lf.left_list.append_array(right_facing)

	for lf in top_facing:
		lf.top_list.append_array(bottom_facing)

	
	t_bottom.left_list.append(t_bottom)
	t_bottom.right_list.append(t_bottom)
	
	t_top.left_list.append(t_top)
	t_top.right_list.append(t_top)
	
	t_left.top_list.append(t_left)
	t_left.bottom_list.append(t_left)
	
	t_right.top_list.append(t_right)
	t_right.bottom_list.append(t_right)

	t_outside_empty.top_list.append_array([t_bottom, t_inside_lr, t_inside_ll, t_outside_empty])
	t_outside_empty.bottom_list.append_array([t_top, t_inside_ur, t_inside_ul, t_outside_empty])
	t_outside_empty.left_list.append_array([t_right, t_inside_ur, t_inside_lr, t_outside_empty])
	t_outside_empty.right_list.append_array([t_left, t_inside_ul, t_inside_ll, t_outside_empty])
	
	t_inside_empty.top_list.append_array([t_inside_empty, t_top])
	t_inside_empty.bottom_list.append_array([t_inside_empty, t_bottom])
	t_inside_empty.left_list.append_array([t_inside_empty, t_left])
	t_inside_empty.right_list.append_array([t_inside_empty, t_right])
	
	t_inside_ur.left_list.append(t_top)
	t_inside_ur.bottom_list.append_array([t_right, t_inside_lr])
	
	t_inside_ul.right_list.append(t_top)
	t_inside_ul.bottom_list.append_array([t_left, t_inside_ll])
	
	t_inside_lr.left_list.append(t_bottom)
	t_inside_lr.top_list.append_array([t_right, t_inside_ur])
	
	t_inside_ll.right_list.append(t_bottom)
	t_inside_ll.top_list.append_array([t_left, t_inside_ul])

	t_outside_ur.left_list.append(t_bottom)
	t_outside_ur.bottom_list.append_array([t_left, t_outside_lr])
	t_outside_ur.top_list.append(t_inside_empty)
	t_outside_ur.right_list.append(t_inside_empty)
	
	t_outside_ul.right_list.append(t_bottom)
	t_outside_ul.bottom_list.append_array([t_right, t_outside_ll])
	t_outside_ul.top_list.append(t_inside_empty)
	t_outside_ul.left_list.append(t_inside_empty)
	
	t_outside_lr.left_list.append(t_top)
	t_outside_lr.top_list.append_array([t_left, t_outside_ur])
	t_outside_lr.bottom_list.append(t_inside_empty)
	t_outside_lr.right_list.append(t_inside_empty)
	
	
	t_outside_ll.right_list.append(t_top)
	t_outside_ll.top_list.append_array([t_right, t_outside_ul])
	t_outside_ll.bottom_list.append(t_inside_empty)
	t_outside_ll.left_list.append(t_inside_empty)
	

	var wfc = Wfc.new(list, width, height, rng)
	# Pick position for start landing pad
	
	wfc.resolve_init_tile(Vector2i(4, 3), t_outside_empty)
	wfc.resolve_init_tile(Vector2i(4, 4), t_top)
	
	# Make the boarder all t_inside_empty
	for y in range(0, height):
		for x in range(0, width):
			if x == 0 or x == width - 1 or y == 0 or y == height - 1:
				wfc.resolve_init_tile(Vector2i(x, y), t_inside_empty)
			# Make it harder for the WFC to put us into a box at start
			if x > 5 and y > 5 and x < width - 3 and y < height -3 and x == y:
				wfc.resolve_init_tile(Vector2i(x, y), t_outside_empty)
			
	
	var bad_gen_count = wfc.generate_until_good()
	if bad_gen_count > 0:
		print("Bad gen Count: ", bad_gen_count)
	
	
	for y in range(0, width):
		for x in range(0, height):
			var pos = Vector2i(x, y)
			var tile = wfc.get_tile(pos)
			if tile != null:
				var inst = tile.ref.call(pos)
				if inst != null:
					random_tiles[_get_index_of(pos)] = inst
	
	const middle_height = 128
	# Finish Random tiles:
	var pos :Vector2i = Vector2i.ZERO
	for i in range(0, width * height):
		var tile = random_tiles[i]
		if tile is MapSectionRandom:
			var left_index  = _get_index_of(pos + Vector2i(-1, 0))
			var right_index = _get_index_of(pos + Vector2i(1, 0))
			var left_tile = null
			if left_index >= 0:
				left_tile = random_tiles[left_index]
			var right_tile = null
			if right_index >= 0:
				right_tile = random_tiles[right_index]
			
			if left_tile is MapSectionRandom:
				tile.first_height = left_tile.last_height
			else:
				tile.first_height = middle_height

			if right_tile is MapSectionRandom:
				tile.last_height = right_tile.first_height
			else:
				tile.last_height = middle_height
			if pos == Vector2i(4, 4):
				# Special for first landing pad
				tile.outpost_scene = _ps_outpost_big
			elif tile.scale.y > 0 and rng.randf_range(0, 1) < 0.33:
				# TODO: make this more varied. Probably should have a
				#       pre-created list of outposts.
				tile.outpost_scene = _ps_outpost_medium
		pos.x = pos.x + 1
		if pos.x >= width:
			pos.x = 0
			pos.y = pos.y + 1
			
	for tile in random_tiles:
		if tile != null:
			if tile is MapSectionRandom:
				tile.fill_in_rest()
			add_child(tile)


# Called when the node enters the scene tree for the first time.
func _ready():
	_generate()
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
