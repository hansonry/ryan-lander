extends Object
class_name Wfc

var _tiles : Array[WfcTile] = []
var _byte_count_per_tile : int = 0
var _width : int = 0
var _heigth : int = 0
var _bit_map_size: int = 0
var _map_size: int = 0
var _last_byte_mask: int = 0
var _bit_map : PackedByteArray = PackedByteArray()
var _bit_map_init : PackedByteArray = PackedByteArray()
var _cached_entropy: PackedInt32Array = PackedInt32Array()
var _index_sorted_by_entropy: Array = []
var _lowest_entropy_count: int = 0
var _rng : RandomNumberGenerator = RandomNumberGenerator.new()

var _allowed_top_bottom_lookup : Array[bool] = []
var _allowed_left_right_lookup : Array[bool] = []


var _iteration_count: int = 0

var _bit_map_dx: int = 0
var _bit_map_dy: int = 0

var _bad_map_gen: bool = false
var _bad_map_gen_count : int = 0

func _get_index_for_tile(tile: WfcTile) -> int:
	for i in range(0, _tiles.size()):
		if _tiles[i] == tile:
			return i
	return -1

func _allowed_index_unsided(x : int, y: int) -> int:
	return x + y * _tiles.size()

func _allowed_index(x : int, y: int) -> int:
	#if x >= y:
	#	return _allowed_index_unsided(x, y)
	return _allowed_index_unsided(x, y)

func _compute_allowed_size(size : int) -> int:
	return size * size


func _reset():
	# Copy init map over
	for i in range(0, _bit_map_size):
		_bit_map[i] = _bit_map_init[i]
	# update entropy map based on new map data
	for i in range(0, _map_size):
		var bit_map_index = i * _byte_count_per_tile
		_update_entropy(bit_map_index)
	# Reset sorted entropy list
	_index_sorted_by_entropy.resize(_map_size)
	var x : int = 0
	var y : int = 0
	for i in range(0, _map_size):
		_index_sorted_by_entropy[i] = { 
			"index":  i * _byte_count_per_tile,
			"entropy_index": i,
			"pos": Vector2i(x, y)
		}
		x = x + 1
		if x >= _width:
			x = 0
			y = y + 1
	_prune_sort_and_update_entropy_list()
	_iteration_count = 0
	_bad_map_gen = false



	
func _init(tiles: Array[WfcTile], width : int, height: int, rng : RandomNumberGenerator = null):
	_tiles = tiles
	_width = width
	_heigth = height
	_byte_count_per_tile = ceil(_tiles.size() / 8.0)
	_map_size = _width * _heigth
	_bit_map_size = _map_size * _byte_count_per_tile
	_bit_map_dx = _byte_count_per_tile
	_bit_map_dy = _bit_map_dx * _width
	_bit_map.resize(_bit_map_size)
	_bit_map_init.resize(_bit_map_size)
	_bit_map_init.fill(0xFF)
	_cached_entropy.resize(_map_size)
	_last_byte_mask = 0xFF
	if _byte_count_per_tile * 8 > _tiles.size():
		var diff = _byte_count_per_tile * 8 - _tiles.size()
		_last_byte_mask = _last_byte_mask >> diff
		
	if _last_byte_mask != 0xFF:
		for i in range(0, _bit_map_size, _byte_count_per_tile):
			var index = i + _byte_count_per_tile - 1
			_bit_map_init[index] = _last_byte_mask
	# Create lookup table
	var lookup_table_size = _compute_allowed_size(_tiles.size())
	_allowed_top_bottom_lookup.resize(lookup_table_size)
	_allowed_top_bottom_lookup.fill(false)
	_allowed_left_right_lookup.resize(lookup_table_size)
	_allowed_left_right_lookup.fill(false)

	for i in range(0, _tiles.size()):
		for t in _tiles[i].top_list:
			var other_index = _get_index_for_tile(t)
			_allowed_top_bottom_lookup[_allowed_index(i, other_index)] = true
		for b in _tiles[i].bottom_list:
			var other_index = _get_index_for_tile(b)
			_allowed_top_bottom_lookup[_allowed_index(other_index, i)] = true
		for l in _tiles[i].left_list:
			var other_index = _get_index_for_tile(l)
			_allowed_left_right_lookup[_allowed_index(i, other_index)] = true
		for r in _tiles[i].right_list:
			var other_index = _get_index_for_tile(r)
			_allowed_left_right_lookup[_allowed_index(other_index, i)] = true


	if rng != null:
		_rng = rng
		

func _create_tile_index_list_from_index(bit_map :PackedByteArray, index: int) -> Array[int]:
	var out : Array[int] = []
	var tiles_index : int = 0
	for i in range(index, index + _byte_count_per_tile):
		var val: int = bit_map[i]
		for k in range(0, 8):
			if (val & 1) == 1:
				out.append(tiles_index)
			tiles_index = tiles_index + 1
			val = val >> 1
	return out


func _clear_bit_map_index(index: int):
	for i in range(index, index + _byte_count_per_tile):
		_bit_map[i] = 0

func _set_bit_in_array_at_index(array, bit_map_index: int, bit: int):
	@warning_ignore("integer_division")
	var byte_offset = floor(bit / 8)
	var bit_offset = bit - byte_offset * 8
	var byte_index = bit_map_index + byte_offset
	var mask = 1 << bit_offset
	
	var val = array[byte_index]
	val = val | mask
	array[byte_index] = val

	
func _set_bit_map_index(index: int, bit: int):
	_set_bit_in_array_at_index(_bit_map, index, bit)


func _resolve_index(index: int):
	var tile_index_list = _create_tile_index_list_from_index(_bit_map, index)
	var weight_list = []
	weight_list.resize(tile_index_list.size())
	var total_weight = 0
	for i in range(0, tile_index_list.size()):
		total_weight = total_weight + _tiles[tile_index_list[i]].weight
		weight_list[i] = total_weight
	var rand_value = _rng.randf_range(0, total_weight)
	var tile_index = weight_list.bsearch(rand_value)
	_clear_bit_map_index(index)
	_set_bit_map_index(index, tile_index_list[tile_index])

func _get_bit_map_index_from_pos(pos: Vector2i) -> int:
	if pos.x < 0 or pos.x >= _width or pos.y < 0 or pos.y >= _heigth:
		return -1
	return pos.x * _bit_map_dx + pos.y * _bit_map_dy

func _is_in_range_of_map(bit_map_index: int) -> bool:
	return bit_map_index >= 0 and bit_map_index < _bit_map_size

func _get_array_from_bit_map_index(bit_map_index: int) -> Array[int]:
	var out : Array[int] = []
	out.resize(_byte_count_per_tile)
	for i in range(0, _byte_count_per_tile):
		out[i] = _bit_map[i + bit_map_index]
	return out


	

func _compute_allowed_bits_from_types(allowed: Array[int]) -> Array[int]:
	var out: Array[int] = []
	out.resize(_byte_count_per_tile)
	out.fill(0)
	for a in allowed:
		_set_bit_in_array_at_index(out, 0, a)
	return out

func _and_into(dest, dest_bit_index: int, src, src_bit_index : int) -> bool:
	var changed : bool = false
	var all_zero: bool = true
	#var was = dest.slice(dest_bit_index, dest_bit_index + _byte_count_per_tile)
	for i in range(0, _byte_count_per_tile):
		var dest_index = i + dest_bit_index
		var src_index = i + src_bit_index
		var prev_dest = dest[dest_index]
		dest[dest_index] = dest[dest_index] & src[src_index]
		if prev_dest != dest[dest_index]:
			changed = true
		if dest[dest_index] != 0:
			all_zero = false
	#if all_zero and changed:
	#	breakpoint
	return changed

func _compute_entropy(array, bit_map_index: int) -> int:
	var out : int = 0
	for i in range(bit_map_index, bit_map_index + _byte_count_per_tile):
		var val = array[i];
		for k in range(0, 8):
			if (val & 1) == 1:
				out = out + 1
			val = val >> 1
	return out

func _update_entropy(bit_map_index: int):
	var entropy = _compute_entropy(_bit_map, bit_map_index)
	@warning_ignore("integer_division")
	_cached_entropy[bit_map_index / _byte_count_per_tile] = entropy

func _limmit_bit_map_index_to(bit_map: PackedByteArray, bit_map_index: int, allowed: Array[int]) -> bool:
	var allowed_cell_data = _compute_allowed_bits_from_types(allowed)
	var changed = _and_into(bit_map, bit_map_index, allowed_cell_data, 0)
	if changed and bit_map == _bit_map:
		_update_entropy(bit_map_index)
	return changed

func _add_to_index_set_primary(the_set: Array[int], allowed_array: Array[bool], tile_index: int):
	for i in range(0, _tiles.size()):
		if allowed_array[_allowed_index(tile_index, i)] and not the_set.has(i):
			the_set.append(i)

func _add_to_index_set_secondary(the_set: Array[int], allowed_array: Array[bool], tile_index: int):
	for i in range(0, _tiles.size()):
		if allowed_array[_allowed_index(i, tile_index)] and not the_set.has(i):
			the_set.append(i)


func _propagate_change_from_bit_map_index( bit_map : PackedByteArray, tile_index: int, tile_position : Vector2i):
	var tile_index_list = _create_tile_index_list_from_index(bit_map, tile_index)
	if tile_index_list.size() == 0:
		_bad_map_gen = true
	else:
		var top_set: Array[int] = []
		var bottom_set: Array[int] = []
		var left_set: Array[int] = []
		var right_set: Array[int] = []
		for i in tile_index_list:
			_add_to_index_set_primary(top_set, _allowed_top_bottom_lookup, i)
			_add_to_index_set_secondary(bottom_set, _allowed_top_bottom_lookup, i)
			_add_to_index_set_primary(left_set, _allowed_left_right_lookup, i)
			_add_to_index_set_secondary(right_set, _allowed_left_right_lookup, i)

		var operations = [
			{ "offset": Vector2i( 1,  0), "allowed_set": right_set  },
			{ "offset": Vector2i(-1,  0), "allowed_set": left_set   },
			{ "offset": Vector2i( 0,  1), "allowed_set": bottom_set },
			{ "offset": Vector2i( 0, -1), "allowed_set": top_set    },
		]
		
		for o in operations:
			var pos = tile_position + o.offset
			var bit_map_index = _get_bit_map_index_from_pos(pos)
			if _is_in_range_of_map(bit_map_index) and _limmit_bit_map_index_to(bit_map, bit_map_index, o.allowed_set):
				_propagate_change_from_bit_map_index(bit_map, bit_map_index, pos)

func _fast_remove(array, index: int):
	var size = array.size()
	if index < size - 1:
		array[index] = array[size - 1]
	array.remove_at(size - 1)

func _prune_sort_and_update_entropy_list():
	# Remove items with no entropy left
	for i in range(_index_sorted_by_entropy.size() - 1, -1, -1):
		var entropy_index = _index_sorted_by_entropy[i].entropy_index
		if _cached_entropy[entropy_index] <= 1:
			_fast_remove(_index_sorted_by_entropy, i)
	
	# Sort by Eentropy
	_index_sorted_by_entropy.sort_custom(func(a, b):
		var a_entropy = _cached_entropy[a.entropy_index]
		var b_entropy = _cached_entropy[b.entropy_index]
		return b_entropy > a_entropy
	)
	# Find lowest entropy count
	_lowest_entropy_count = _index_sorted_by_entropy.size()
	if _lowest_entropy_count > 0:
		var lowest = _cached_entropy[_index_sorted_by_entropy[0].entropy_index]
		for i in range(0, _lowest_entropy_count):
			if _cached_entropy[_index_sorted_by_entropy[i].entropy_index] != lowest:
				_lowest_entropy_count = i + 1
				break

func _iteration():
	var list_index : int = _rng.randi_range(0, _lowest_entropy_count - 1)
	var tile_data = _index_sorted_by_entropy[list_index]
	var tile_index = tile_data.index
	_resolve_index(tile_index)
	_update_entropy(tile_index)
	_propagate_change_from_bit_map_index(_bit_map, tile_index, tile_data.pos)
	_prune_sort_and_update_entropy_list()


func generate():
	_reset()
	while not _index_sorted_by_entropy.is_empty():
		#print(_iteration_count, ", ", _index_sorted_by_entropy.size())
		_iteration()
		_iteration_count = _iteration_count + 1

func generate_until_good() -> int:
	_bad_map_gen_count = -1
	_bad_map_gen = true
	while _bad_map_gen:
		_bad_map_gen_count = _bad_map_gen_count + 1
		generate()
	return _bad_map_gen_count


func get_width():
	return _width

func get_height():
	return _heigth

func get_tile(pos: Vector2i) -> WfcTile:
	var bit_map_index = _get_bit_map_index_from_pos(pos)
	if not _is_in_range_of_map(bit_map_index):
		return null
	var tile_index_list = _create_tile_index_list_from_index(_bit_map, bit_map_index)
	return _tiles[tile_index_list[0]]

func resolve_init_tile(pos: Vector2i, tile: WfcTile):
	var bit_map_index = _get_bit_map_index_from_pos(pos)
	var tile_index = _get_index_for_tile(tile)
	if _is_in_range_of_map(bit_map_index) and tile_index >= 0:
		if _limmit_bit_map_index_to(_bit_map_init, bit_map_index, [tile_index]):
			_propagate_change_from_bit_map_index(_bit_map_init, bit_map_index, pos)
