extends Node2D

@onready var _lander : Lander = $Lander
@onready var _hud : HUD = $HUD


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	_hud.set_player_velocity(_lander.linear_velocity)
	_hud.set_player_fuel(_lander.get_fuel_percent() * 100)
	var coords: Vector2i
	coords.x = floor(_lander.position.x / 1024)
	coords.y = floor(_lander.position.y / 256)
	_hud.set_player_grid_coords(coords)
	pass
