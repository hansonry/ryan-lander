extends RigidBody2D
class_name Lander

@export var thrust_max : float = 500
@export var thrust_small_percent: float = 0.50
@export var torque : float = 500
@export var fuel_max : float = 1000
@export var fuel : float = fuel_max
@export var fuel_rate : float = 50

@onready var _thrust_particles = $ThrustParticles

@onready var start_pos : Vector2 = position
@onready var start_rot : float = rotation
@onready var start_fuel : float = fuel
var thrust_commnad: float = 0
var thrust : float = 0
var is_thrusting : bool = false

# Called when the node enters the scene tree for the first time.
func _ready():
	_thrust_particles.emitting = false
	pass # Replace with function body.

func is_upright():
	var rad = deg_to_rad(1)
	return rotation > -rad and rotation < rad

func are_controls_active() -> bool:
	return (Input.is_action_pressed("rotate_left") or
			Input.is_action_pressed("rotate_right") or
			Input.is_action_pressed("thrust"))

func get_fuel_percent():
	return fuel / fuel_max

func is_fuel_tank_full() -> bool:
	return fuel >= fuel_max

func add_fuel(fuel_to_add: float):
	fuel = fuel + fuel_to_add
	fuel = clamp(fuel, 0, fuel_max)

func _set_thruster_command(percent):
	percent = clamp(percent, 0, 1)
	thrust_commnad = percent


func _update_thrust_power_from_input():
	var percent : float = 0
	if Input.is_action_pressed("thrust"):
		if Input.is_key_pressed(KEY_SHIFT):
			percent = thrust_small_percent;
		else:
			percent = 1;
	_set_thruster_command(percent)

func _input(event):
	if (event.is_action_pressed("thrust") or
		event.is_action_released("thrust") or 
		(event is InputEventKey and event.keycode == KEY_SHIFT)):
		_update_thrust_power_from_input()

func _update_thruster_state(delta):
	
	var max_thrust_by_fuel = thrust_max
	if fuel <= 0:
		max_thrust_by_fuel = 0
	thrust = thrust_max * thrust_commnad 
	thrust = min(thrust, max_thrust_by_fuel)
	var thrust_percent = thrust / thrust_max
	is_thrusting = thrust_percent > 0.01
	
	_thrust_particles.emitting = is_thrusting
	_thrust_particles.amount_ratio = thrust_percent * thrust_percent

	# Fuel Usage
	fuel = fuel - fuel_rate * thrust_percent * delta
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_just_pressed("reset"):
		position = start_pos
		rotation = start_rot
		linear_velocity = Vector2.ZERO
		angular_velocity = 0
		fuel = start_fuel
	_update_thruster_state(delta)
	pass
	
func _get_up_normal() -> Vector2:
	return Vector2(sin(rotation), -cos(rotation))
	
func _physics_process(delta):
	if is_thrusting:
		var direction = _get_up_normal()
		apply_central_force(direction * thrust)
		
	if Input.is_action_pressed("rotate_left"):
		apply_torque(-torque)
	if Input.is_action_pressed("rotate_right"):
		apply_torque(torque)
