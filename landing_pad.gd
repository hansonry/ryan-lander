@tool
extends StaticBody2D
class_name LandingPad

enum State {
	Ready,
	Docking,
	Docked,
	Complete,
}

var _timer : float = 0
var state : State = State.Ready
var _lander: Lander = null
var _bad_park : bool = false

@export var width: int = 10 :
	set(value):
		width = value
		_editor_update()
@export var refuel_rate: float = 100
@export var docking_timeout_seconds : float = 1



@export var color_ready     : Color = Color(1, 1, 1)
@export var color_docking   : Color = Color(1, 1, 0)
@export var color_docked    : Color = Color(0, 1, 0)
@export var color_completed : Color = Color(0, 0, 1)

@onready var _col_shape_center:        CollisionShape2D   = $CollisionShape2DCenter
@onready var _col_shape_right:         CollisionPolygon2D = $CollisionPolygon2DRight
@onready var _col_shape_land:          CollisionShape2D   = $Area2DLanderSensor/CollisionShape2D
@onready var _col_shape_parking_right: CollisionShape2D   = $Area2DParkingLines/CollisionShape2DRight
@onready var _left:   Sprite2D = $Left
@onready var _center: Sprite2D = $Center
@onready var _right:  Sprite2D = $Right


# Called when the node enters the scene tree for the first time.
func _ready():
	_editor_update()
	pass # Replace with function body.

func _editor_update():
	if is_node_ready():
		_center.scale.x = width
		_right.position.x = 4 + width
		
		_col_shape_center.shape.size.x =  width
		_col_shape_center.position.x = width / 2 + 4
		_col_shape_right.position.x = width + 8
		
		_col_shape_land.shape.size.x = width / 2
		_col_shape_land.position.x = width / 2 + 4
		_col_shape_parking_right.position.x = width + 9

func _set_pad_color(color: Color):
	_left.modulate = color
	_center.modulate = color
	_right.modulate = color

func _game_process(delta):
	if _lander != null and not _bad_park and _lander.is_upright():
		if state == State.Ready:
			state = State.Docking
			_timer = 0
		elif state == State.Docking:
			if _timer >= docking_timeout_seconds:
				state = State.Docked
			elif _lander.are_controls_active():
				_timer = 0
		elif state == State.Docked:
			if _lander.are_controls_active():
				state = State.Ready
				_timer = 0
			elif _lander.is_fuel_tank_full() or refuel_rate < 0.0001:
					state = State.Complete
			else:
				var fuel_xfer = refuel_rate * delta
				_lander.add_fuel(fuel_xfer)
	else:
		state = State.Ready
		
	_timer = _timer + delta
	
	match state:
		State.Ready:
			_set_pad_color(color_ready)
		State.Docking:
			_set_pad_color(color_docking)
		State.Docked:
			_set_pad_color(color_docked)
		State.Complete:
			_set_pad_color(color_completed)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if not Engine.is_editor_hint():
		_game_process(delta)
	pass

func _lander_event(lander : Lander, entered : bool):
	if entered:
		_lander = lander
	else:
		_lander = null
		


func _on_area_2d_body_entered(body):
	if body is Lander:
		_lander_event(body, true)


func _on_area_2d_body_exited(body):
	if body is Lander:
		_lander_event(body, false)


func _on_area_2d_parking_lines_body_entered(body):
	if body is Lander:
		_bad_park = true
	pass # Replace with function body.


func _on_area_2d_parking_lines_body_exited(body):
	if body is Lander:
		_bad_park = false
	pass # Replace with function body.
